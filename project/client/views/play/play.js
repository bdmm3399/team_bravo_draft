/*****************************************************************************/
/* Play: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Play.events ({

  'click #playerResponseButton': function (event, template) { //This helped me: http://stackoverflow.com/questions/28034512/dynamic-radio-button-creation-using-handlebars-in-meteor
    event.preventDefault ();
    var playerResponseCode = $ (":radio[name=playerResponsesRadio]:checked").val ();
    console.log ('playerResponseCode: ' + playerResponseCode);

    Meteor.call ('/app/processPlay', Session.get ('SCENARIO'), playerResponseCode, function (err, response) {
      console.log (JSON.stringify (response));
      Session.set ('result', JSON.stringify (response));

      var score = 0;

      if (Meteor.user().profile.score != undefined) {
        score = Meteor.user().profile.score;

      }

      Session.set('USER_SCORE',score);
      Session.set('TRACKER',tracker);

      if (response.playResult != null) {

        alert (response.playResult);
      }

      // if new scenario, set new scenario
      // if new npCharacterRequest, set new npCharacterRequest
      // newScenario:null,newNPCharacter:null,newNPCharacterRequest:null
      if (response.playPath != null) {

        Template.Play.setScenario (response.playPath.outScenarioCode);

      } else if (response.scenario === true) {

        Template.Play.scenario ();
      }

      if (err) {
        alert (err);
      }
    });
  }
});

Template.Play.helpers ({
  scenario: function () {

    var scenario = Session.get ('SCENARIO');
    return scenario;
  },
  npCharacter: function () {

    var character = Session.get ('NPCHARACTER');
    return character;
  },
  npCharacterRequest: function () {

    var request = Session.get ('NPCHARACTER_REQUEST');
    return request;

  },
  playerResponses: function () {

    var responses = Session.get ('PLAYER_RESPONSES');
    return responses;
  },
  userScore: function () {

    return Session.get ('USER_SCORE') ;

  },
  playResult: function () {

    return Session.get ('PLAY_RESULT') ;

  }
});

Template.Play.prepScenario = function (tracker) {
  // Random Scenario
  var scenarios = Scenario.find ({}).fetch ();//makes array of scenarios

  var Index = tracker;

  var scenario = scenarios[Index];

  console.log ('randomScenario: ' + JSON.stringify (scenario));

  // Random Character
  /*var npCharacters = NPCharacter.find ({scenarioCodes: scenario.objectCode}).fetch ();

  console.log ('npCharacters: ' + JSON.stringify (npCharacters));

  Index = Math.floor ((Math.random () * npCharacters.length));

  var randomCharacter = npCharacters [Index];

  console.log ('randomCharacter: ' + JSON.stringify (randomCharacter));
  */
  //var requests = NPCharacterRequest.find ({objectCode: {$in: scenarios.npCharacterRequestCodes}}).fetch ();
  var requests = NPCharacterRequest.find({Scenario: scenario.npCharacterRequestCodes}).fetch ();

  console.log ('requests: ' + JSON.stringify (requests));

  //Index = Math.floor ((Math.random () * requests.length));

 // var myRequest = requests[Index];

  //console.log ('randomRequest: ' + JSON.stringify (myRequest));

  Template.Play.setScenario (scenario, requests);

}

Template.Play.setScenario = function (scenario, request) {

  Session.set ('SCENARIO', scenario);
  //Session.set ('NPCHARACTER', character);
  Session.set ('NPCHARACTER_REQUEST', request);


  var playerResponses = PlayerResponse.find ({npCharacterRequestCodes: request.playerResponseCodes}).fetch ();

  Session.set ('PLAYER_RESPONSES', playerResponses);

  console.log ('playerResponses: ' + JSON.stringify (playerResponses));
}

/*****************************************************************************/
/* Play: Lifecycle Hooks */
/*****************************************************************************/
Template.Play.created = function () {

  var score = 0;
  var tracker = 0;

  if (Meteor.user().profile.score != undefined) {
    score = Meteor.user().profile.score;

  }

  if (Meteor.user().profile.tracker != undefined) {
    tracker = Meteor.user().profile.tracker;

  }

  Session.set('USER_SCORE',score);
  Session.set('TRACKER',tracker);

  Template.Play.prepScenario(tracker);

};

Template.Play.rendered = function () {


};

Template.Play.destroyed = function () {
};
