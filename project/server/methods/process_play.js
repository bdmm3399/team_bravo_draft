/*****************************************************************************/
/* ProcessPlay Methods */
/*****************************************************************************/

Meteor.methods({
 '/app/processPlay': function (scenario,playerResponseCode) {

  var user = Meteor.user();

  var score = user.profile.score;

  var tracker = user.profile.tracker;

  if (tracker == undefined) {

   tracker = 0;

  }

  if (score == undefined) {

   score = 0;

  }

  var resultMessage;

  // Evaluate input elements
  // In this case, results are completely random
  //var randomResult = Math.random() < 0.5 ? true : false; // http://stackoverflow.com/questions/9730966/how-to-decide-between-two-numbers-randomly-using-javascript



  //if (randomResult === true) {
   resultMessage = "The story continues...";
   // If necessary increment user's score
   score ++;
  //} else {
   //resultMessage = "What happens next...";
   //score --;
  //}

  var playPath = PlayPath.findOne({$and:[
                                          {inPlayerResponseCode: playerResponseCode},
                                          {inScenarioCode: scenario.objectCode}
                                        ]});

  tracker ++;

  // If necessary select a

  // new scenario,
  // var scenarios = Scenario.find ({}).fetch ();

  // character,
  // var npCharacters = NPCharacter.find ({scenarioCodes: randomScenario.objectCode}).fetch ();

  // and request,
  // var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

  // and return them to the template


  Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.score":score}});
  Meteor.users.update({_id:Meteor.user()._id}, {$set:{"profile.tracker":tracker}});

   return {randomScenario:true,playResult:resultMessage,playPath:playPath};

 }

});