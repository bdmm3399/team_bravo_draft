/**
 * Created by vincilbishop on 2/25/15.
 */

Meteor.startup(function() {

  // Scenario
  var scenarioText = Assets.getText ("db/scenarios.json");
  var scenarios = JSON.parse (scenarioText);
  scenarios.forEach (function (scenario) {
    var exists = typeof Scenario.findOne ({ objectCode: scenario.objectCode })  === 'object';

    if (!exists) {

      Scenario.insert(scenario);
    }
  });

  /*// NPCharacter
  var npCharacterText = Assets.getText ("db/npCharacters.json");
  var npCharacters = JSON.parse (npCharacterText);
  npCharacters.forEach (function (npCharacter) {
    var exists = typeof NPCharacter.findOne ({ objectCode: npCharacter.objectCode })  === 'object';

    if (!exists) {

      NPCharacter.insert(npCharacter);
    }
  });*/

  // NPCharacter Request
  var npCharacterRequestText = Assets.getText ("db/npCharacterRequests.json");
  var npCharacterRequests = JSON.parse (npCharacterRequestText);
  npCharacterRequests.forEach (function (npCharacterRequest) {
    var exists = typeof PlayerResponse.findOne ({ objectCode: npCharacterRequest.objectCode })  === 'object';

    if (!exists) {

      NPCharacterRequest.insert(npCharacterRequest);
    }
  });

  // NPCharacter Response

  // PlayerRequest

  // PlayerResponse
  var playerResponseText = Assets.getText ("db/playerResponses.json");
  var playerResponses = JSON.parse (playerResponseText);
  playerResponses.forEach (function (playerResponse) {
    var exists = typeof PlayerResponse.findOne ({ objectCode: playerResponse.objectCode })  === 'object';

    if (!exists) {

      PlayerResponse.insert(playerResponse);
    }
  });


  // PlayPath
  var playPathText = Assets.getText ("db/playPaths.json");
  var playPaths = JSON.parse (playPathText);
  playPaths.forEach (function (playPath) {
    var exists = typeof PlayPath.findOne ({ objectCode: playPath.objectCode })  === 'object';

    if (!exists) {

      PlayPath.insert(playPath);
    }
  });


});