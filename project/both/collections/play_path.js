
// A non playable character in the game

PlayPath = new Mongo.Collection('play_path');

PlayPath.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  inPlayerResponseCode:{
    type:[String],
    optional: true
  },
  inScenarioCode:{
    type:String,
    optional: true
  },
  outScenarioCode:{
    type:String,
    optional: true
  }

}));