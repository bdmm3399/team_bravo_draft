Scenario = new Mongo.Collection('scenario');

Scenario.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  scenarioDescription:{
    type:String
  },
  /*npCharacterCodes:{
    type:[String],
    optional: true
  },
  */
  npCharacterRequestCodes:{
    type:String,
    optional: true
  },
  imageCode:{
    type:Number,
    optional: true
  }
  
}));

/*
 * Add query methods like this:
 *  Scenario.findPublic = function () {
 *    return Scenario.find({is_public: true});
 *  }
 */